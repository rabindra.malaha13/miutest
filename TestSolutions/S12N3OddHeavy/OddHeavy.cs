﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S12N3OddHeavy
{
    public class OddHeavy
    {
        public static int IsOddHeavy(int[] array)
        {
            if (!array.Any(x => x % 2 != 0)) return 0;
            if (array.Length == 1) return 1;
            if (array.All(x => x % 2 != 0)) return 1;
            var maxEle = array.Max();
            if (maxEle % 2 == 0) return 0;
            var evenArray = array.Where(x => x % 2 == 0).ToArray();
            var oddArray = array.Where(x => x % 2 != 0).ToArray();
            int minOdd = oddArray.Min();
            foreach (var ele in evenArray)
            {
                if (ele > minOdd) return 0;
            }
            return 1;
        }
    }
}
