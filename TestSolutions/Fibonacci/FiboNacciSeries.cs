﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.Fibonacci
{
    public class FiboNacciSeries
    {
        public static int ClosestFibonacci(int n)
        {
            int fibonacci = 0;
            int f1 = 1;
            int f2 = 1;
            while(fibonacci <= n)
            {
                fibonacci = f1 + f2;
                f1 = f2;
                f2 = fibonacci;
            }
            return f1;
        }
    }
}
