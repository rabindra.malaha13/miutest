﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S6N2BaseSystem
{
    public class BaseSystem
    {
        //Question 2 and 3 in same solution
        public static int BaseNumber(int[] array, int n)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (n < array[i])
                {
                    return 0;
                }
                sum += (int)(array[array.Length - 1 - i] * Math.Pow(n, i));
            }
            return sum;
        }
    }
}
