﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S7N1NonZerolArray
{
    public class NonZeroArray
    {
        public static int CheckNonZeroArray(int[] array)
        {
            foreach (int i in array)
            {
                if (i == 0) return 0;
            }
            return 1;
        }
    }
}
