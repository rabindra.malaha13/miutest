﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S16N3EncodingArray
{
    public class EncodeArray
    {
        public static int EncodeMyArray(int[] array)
        {
            double sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (i == array.Length - 1)
                    break;

                sum += Math.Abs(array[i] - array[i + 1]) * Math.Pow(10, array.Length - 2 - i);
            }
            if (array.FirstOrDefault() <= 0)
                return -1 * (int)sum;
            return (int)sum;
        }
    }
}
