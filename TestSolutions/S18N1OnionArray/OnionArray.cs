﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S18N1OnionArray
{
    public class OnionArray
    {
        public static int IsOnionArray(int[] array)
        {
            if (array.Length <= 1) return 1;
            for (int j = 0; j < array.Length; j++)
            {
                for (int k = 0; k < array.Length; k++)
                {
                    if ((j != k) && (j + k + 1) == array.Length)
                    {
                        int sum = array[j] + array[k];
                        if (sum > 10) return 0;
                    }
                }
            }
            return 1;
        }
    }
}
