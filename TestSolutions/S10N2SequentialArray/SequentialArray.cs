﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S10N2SequentialArray
{
    public class SequentialArray
    {
        public static int IsSequentialArray(int[] array)
        {
            if (array == null) return 1;
            if (array.Contains(0)) return 0;
            int[] newArrayEle = array.Distinct().ToArray();
            int first = newArrayEle.First();
            bool check = newArrayEle.Skip(1).All(x =>
            {
                bool u = first.CompareTo(x) < 0;
                return u;
            });
            if (!check) return 0;
            foreach (int ele in newArrayEle)
            {
                int count = array.Count(x => x == ele);
                if (count >= ele) return 0;
            }
            return 1;
        }
    }
}
