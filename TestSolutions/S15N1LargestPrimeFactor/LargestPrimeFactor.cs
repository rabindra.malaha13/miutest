﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S15N1LargestPrimeFactor
{
    public class LargestPrimeFactor
    {
        public static int LargestPF(int n)
        {
            if (n <= 1) return 0;
            int largestPF = 0;
            int[] factors = { };
            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                    factors = factors.Append(i).ToArray();
            }

            for (int j = factors.Length - 1; j >= 0; j--)
            {
                int num = factors[j];
                for (largestPF = 2; largestPF < num; largestPF++)
                {
                    if (num % largestPF == 0)
                        break;
                }
                if (largestPF == num)
                {
                    break;
                }
            }
            return largestPF;
        }
    }
}
