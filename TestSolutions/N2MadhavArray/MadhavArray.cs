﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.N2MadhavArray
{
    public class MadhavArray
    {
        public static int isMadhavArray(int[] a)
        {
            int isMadhave = 1;
            if (a.Length < 3) { return 0; }
            // check if The length of a Madhav array must be n*(n+1)/2 for some n.
            int isLengthisMadhav = 0;
            for (int i = 1; i <= a.Length && isLengthisMadhav == 0; i++)
            {
                if (a.Length == i * (i - 1) / 2)
                {
                    isLengthisMadhav = 1;
                }
            }
            if (isLengthisMadhav == 0) return 0;// the length not satisfy the condition

            int startindex = 1, round = 1;
            int firstvalue = a[0];
            int endindex = startindex + round;
            while (isMadhave == 1 && endindex <= a.Length)
            {
                int sum = 0;
                for (int i = startindex; i <= endindex; i++)
                {
                    sum += a[i];
                }
                if (firstvalue != sum)
                    isMadhave = 0;

                round++;
                startindex = endindex + 1;
                endindex = startindex + round;
            }
            //
            return isMadhave;
        }
    }
}
