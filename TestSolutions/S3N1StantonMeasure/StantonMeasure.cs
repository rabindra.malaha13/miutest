﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S3N1StantonMeasure
{
    public class StantonMeasure
    {
        public static int CountStantonMeasure(int[] array)
        {
            int count = 0;
            int fCount = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 1) { fCount++; }
            }
            int next = fCount;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == next) { count++; }
            }
            return count;
        }
    }
}
