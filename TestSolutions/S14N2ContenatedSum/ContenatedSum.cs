﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S14N2ContenatedSum
{
    public class ContenatedSum
    {
        public static int CheckContenatedSum(int n, int l)
        {
            int[] numberArray = NumberArray(n);
            double sum = 0;
            for (int i = numberArray.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < l; j++)
                {
                    sum += numberArray[i] * Math.Pow(10, j);
                }
            }
            if (sum != n) return 0;
            return 1;
        }

        public static int[] NumberArray(int n)
        {
            int[] array = { };
            while (n > 0)
            {
                int rem = n % 10;
                n /= 10;
                array = array.Append(rem).ToArray();
            }
            return array;
        }
    }

}