﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S2N3GuthrieSequence
{
    public class GuthrieSequence
    {
        public static int IsGuthrieSequence(int[] array)
        {
            if (array[0] <= 0 || array[array.Length - 1] != 1) return 0;

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] % 2 == 0)
                {
                    if (array[i + 1] != array[i] / 2)
                    {
                        return 0;
                    }
                }
                else
                {
                    if (array[i + 1] != (array[i] * 3 + 1))
                    {
                        return 0;
                    }
                }
            }
            return 1;
        }
    }
}
