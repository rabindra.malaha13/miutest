﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S5N2Divisible
{
    public class Divisible
    {
        public static int IsDivisible(int[] array, int n)
        {
            if (array.Length == 0) return 1;
            foreach (int i in array)
            {
                if (i % n != 0) return 0;
            }
            return 1;
        }
    }
}
