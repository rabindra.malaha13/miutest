﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S8N1MatchArray
{
    public class MatchArray
    {
        public static int Matches(int[] A, int[] P)
        {
            if (A.Length != P.Sum(x => Math.Abs(x))) return 0;
            int[] cloneArray = A;
            for (int i = 0; i < P.Length; i++)
            {
                if (Math.Abs(P[i]) > cloneArray.Length) return 0;
                if (P[i] > 0)
                {
                    foreach (int j in cloneArray.Take(P[i]))
                    {
                        if (j < 0) return 0;
                    }
                }
                else
                {
                    foreach (int j in cloneArray.Take(Math.Abs(i)))
                    {
                        if (j > 0) return 0;
                    }
                }
                cloneArray = cloneArray.Skip(Math.Abs(P[i])).ToArray();
            }

            return 1;
        }
    }
}
