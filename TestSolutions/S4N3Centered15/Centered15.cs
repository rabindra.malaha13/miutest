﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S4N3Centered15
{
    public class Centered15
    {
        public static bool IsCentered15(int[] array)
        {
            if (array.Length % 2 == 0) return false;
            bool positiveElement = true;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                {
                    positiveElement = false;
                    break;
                }
            }
            if (positiveElement)
            {
                if (array.Sum() <= 15) return false;
            }

            int centerIndex = (array.Length / 2) + 1;
            int[] firstArray = array.Take(array.Length / 2).ToArray();
            int[] secondArray = array.Skip(centerIndex).ToArray();
            int sum = array[centerIndex - 1];
            if (array.Length == 3 & sum == 15) return true;
            for (int i = 0; i < firstArray.Length; i++)
            {
                sum += firstArray[firstArray.Length - 1 - i] + secondArray[i];
                if (sum == 15) return true;
            }
            return false;
        }
    }
}
