﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.N1NextPerfectSquare
{
    public static class NextPerfectSquare
    {
        public static int IsPerfectSquare(int N)
        {
            int n = (int)(Math.Floor(Math.Sqrt(N)));
            if (n > 0)
            {
                return (int)(Math.Pow(n + 1, 2));
            }
            return 0;
        }
    }
}
