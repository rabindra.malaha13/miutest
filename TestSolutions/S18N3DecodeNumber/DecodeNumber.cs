﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S18N3DecodeNumber
{
    public class DecodeNumber
    {
        public static int[] EncodeArray(int n)
        {
            int[] result = { };
            if (n == 0) return result.Append(1).ToArray();
            if (n < 0)
            {
                result = result.Append(-1).ToArray();
                n = -1 * n;
            }
                
            int[] eleArray = { };
            while (n > 0)
            {
                int rem = n % 10;
                n /= 10;
                eleArray = eleArray.Append(rem).ToArray();
            }
            for (int i = eleArray.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < eleArray[i]; j++)
                {
                    result = result.Append(0).ToArray();
                }
                result = result.Append(1).ToArray();
            }
            return result;
        }
    }
}
