﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S10N3MinMaxdisjoined
{
    public class MinMaxDisjoint
    {
        public static int IsMinMaxDisjoint(int[] array)
        {
            if (array.Length == 2) return 0;
            if (array.Max() == array.Min()) return 0;

            if (array.Count(x => x == array.Max()) > 1)
                return 0;

            if (array.Count(x => x == array.Min()) > 1)
                return 0;

            int maxIndex = Array.IndexOf(array, array.Max());
            int minIndex = Array.IndexOf(array, array.Min());

            if (Math.Abs(maxIndex - minIndex) == 1) return 0;

            return 1;
        }
    }
}
