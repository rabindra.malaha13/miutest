﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S5N3NUniqueArray
{
    public class UniqueArray
    {
        public static int isNUnique(int[] a, int n)
        {
            int count = 0, rtnVal = 0;
            if (a.Length > 1)
            {
                for (int i = 0; i < a.Length - 1; i++)
                {
                    for (int j = i + 1; j < a.Length; j++)
                    {
                        if (a[i] + a[j] == n)
                        {
                            count++;
                        }
                    }
                }
                if (count == 1)
                {
                    rtnVal = 1;
                }
            }
            return rtnVal;
        }
    }
}
