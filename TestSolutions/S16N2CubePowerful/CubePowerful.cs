﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S16N2CubePowerful
{
    public class CubePowerful
    {
        public static int IsCubePowerful(int input)
        {
            int[] array = { };
            int n = input;
            while (n > 0)
            {
                int rem = n % 10;
                array = array.Append(rem).ToArray();
                n /= 10;
            }

            double sum = 0;
            foreach (int i in array)
            {
                sum += Math.Pow(i, 3);
            }
            if (sum != input) return 0;
            return 1;
        }
    }
}
