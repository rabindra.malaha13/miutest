﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S2N1PerfectSquarePair
{
    public class PerfectSquarePair
    {
        public static int PerfectSquarePairCount(int[] array)
        {
            int count = 0;
            int[] firstArray = array.Take(array.Length / 2).ToArray();
            int[] secondArray = array.Skip(array.Length / 2).ToArray();
            foreach (int i in firstArray)
            {
                if (i > 0)
                {
                    foreach (int j in secondArray)
                    {
                        if (i != j && j > 0)
                        {
                            double result = Math.Sqrt(i + j);
                            if (result % 1 == 0)
                            {
                                count++;
                            }
                        }
                    }
                }
            }
            return count;
        }
        public static int IsPerfectSquarePair(int[] array)
        {
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                int x = array[i];
                if (x > 0)
                {
                    for (int j = array.Length - 1; j >= 0; j--)
                    {
                        int y = array[j];
                        if (y > 0 && x != y)
                        {
                            double result = Math.Sqrt(x + y);
                            if (result % 1 == 0)
                            {
                                count++;
                            }
                        }
                    }
                }

            }
            return count / 2; //Divided by two because the process is symmetrical
        }
    }
}
