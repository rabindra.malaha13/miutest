﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S17N2DigitIncreasing
{
    public class DigitIncreasing
    {
        public static int IsDigitIncreasing(int myNumber)
        {
            int num = myNumber;
            int expo = 0;
            while (num > 10)
            {
                num /= 10;
                expo++;
            }

            double check_sum = 0;
            double sum = 0;
            double prevSum = 0;

            for (int i = 0; i <= expo; i++)
            {
                check_sum += num * Math.Pow(10, i);
            }

            if (check_sum > myNumber)
                num = num - 1;

            for (int i = 0; i <= expo; i++)
            {
                prevSum += num * Math.Pow(10, i);
                sum += prevSum;
            }
            if (sum != myNumber) return 0;
            return 1;
        }
    }
}
