﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S12N13FullnessQuetiont
{
    public class FullnessQuotient
    {
        public static int IsFullnessQuotient(int n)
        {
            int count = 0;
            if (n < 0) return --count;
            for (int i = 2; i <= 9; i++)
            {
                int number = n;
                bool dontCount = false;
                while (number > 0)
                {
                    int rem = number % i;
                    number /= i;
                    if (rem == 0)
                    {
                        dontCount = true;
                        break;
                    }
                }
                if (!dontCount)
                    count++;
            }
            return count;
        }
    }
}
