﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S19N1SymmetricallyIncreasing
{
    public class SymmetricallyIncreasing
    {
        public static int IsSymmetricallyIncreasing(int[] array)
        {
            int[] remainingArray = array;
            for (int i = 0; remainingArray.Length > 0; i++)
            {
                int[] newArray = remainingArray.Take(i + 1).ToArray();
                if (newArray.Length != i + 1) return 0;
                for (int j = 0; j < i; j++)
                {
                    if (j + 1 != newArray[j])
                    {
                        return 0;
                    }
                }
                remainingArray = remainingArray.Skip(i + 1).ToArray();
            }
            return 1;
        }
    }
}
