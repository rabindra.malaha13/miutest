﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S14N3SequencedArray
{
    public class SequencedArray
    {
        public static int IsSequencedArray(int[] array, int m, int n)
        {
            if (m > n) return 0;
            int[] newArray = { };
            for (int i = m; i <= n; i++)
            {
                if (!array.Contains(i)) return 0;
            }
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < m || array[i] > n) return 0;
                for (int j = i + 1; j < n; j++)
                {
                    if (array[i] > array[j])
                        return 0;
                }
            }
            return 1;
        }
    }
}
