﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.N1PrimeCount
{
    public class PrimeCount
    {
        public static int primeCount(int start, int end)
        {
            int count = 0;
            for (int i = start; i <= end; i++)
            {
                bool check = CheckPrime(i);
                if (check) { count++; }
            }
            return count;
        }

        public static bool CheckPrime(int Number)
        {
            int i;
            for (i = 2; i <= Number - 1; i++)
            {
                if (Number % i == 0)
                {
                    return false;
                }
            }
            if (i == Number)
            {
                return true;
            }
            return false;
        }
    }
}
