﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S13N3FilterArray
{
    public class FilterArray
    {
        public static int[] FindFilterArray(int[] array, int n)
        {
            int[] binaryArray = FindBinary(n);
            int[] result = { };
            foreach (int b in binaryArray)
            {
                if (b >= array.Length) return null;
                result = result.Append(array[b]).ToArray();
            }

            return result;
        }

        public static int[] FindBinary(int n)
        {
            int[] array = { };
            while (n > 0)
            {
                int rem = n % 2;
                n /= 2;
                array = array.Append(rem).ToArray();
            }
            int[] array2 = { };
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 1)
                    array2 = array2.Append(i).ToArray();
            }
            return array2;
        }
    }
}
