﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S14N1LargestAdjacentSum
{
    public class LargestAdjacentSum
    {
        public static int FindLargestAdjacentSum(int[] array)
        {
            int max = array.Max();
            int maxIndex = Array.IndexOf(array, max);
            if (maxIndex == 0)
                return max + array[1];
            if (maxIndex == array.Length - 1)
                return max + array[array.Length - 2];
            if (array[maxIndex - 1] > array[maxIndex + 1])
            {
                return max + array[maxIndex - 1];
            }
            else
            {
                return max + array[maxIndex + 1];
            }
        }
    }
}
