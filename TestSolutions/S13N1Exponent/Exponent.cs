﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S13N1Exponent
{
    public class Exponent
    {
        public static int GetExponent(int n, int p)
        {
            if (p <= 1) return -1;
            int exp = 0;
            int rem = n % (int)(Math.Pow(p, exp));
            while (rem == 0)
            {
                rem = n % (int)(Math.Pow(p, exp));
                int num = n / (int)(Math.Pow(p, exp));
                if (num == 1) break;
                if (rem != 0)
                {
                    exp--;
                    break;
                }
                exp++;
            }
            return exp;
        }
    }
}
