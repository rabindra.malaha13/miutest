﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.N3Inertial
{
    public class Inertial
    {
        public static bool IsInertial(int[] array)
        {
            int[] oddArray = { };
            int[] evenArray = { };
            if (array.Length == 1) return false;
            if (array.Max() % 2 != 0)
            {
                return false;
            }
            foreach (var i in array)
            {

                if (i % 2 != 0)
                {
                    oddArray = oddArray.Append(i).ToArray();
                }
                else if (i != array.Max())
                {
                    evenArray = evenArray.Append(i).ToArray();
                }
            }
            foreach (var odd in oddArray)
            {
                foreach (var even in evenArray)
                {
                    if (even > odd)
                    {
                        return false;
                    }
                }
            }
            if (oddArray.Length == 0) { return false; }
            return true;
        }
    }
}
