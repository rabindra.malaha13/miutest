﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S7N2DepthNumber
{
    public class DepthNumber
    {
        public static int NumberOfDepth(int n)
        {
            int[] numberArray = { };
            int depth = 0;
            int i = 0;
            while (numberArray.Length < 10)
            {
                i++;
                int prod = i * n;
                string nString = prod.ToString();
                int count = nString.Length;

                while (count > 1)
                {
                    int number = Number(prod, Math.Pow(10, count - 1));
                    int mod = Mod(prod, Math.Pow(10, count - 1));
                    if (!numberArray.Contains(mod) && (mod.ToString()).Length == 1)
                        numberArray = numberArray.Append(mod).ToArray();
                    if (!numberArray.Contains(number) && (number.ToString()).Length == 1)
                        numberArray = numberArray.Append(number).ToArray();
                    prod = mod;
                    count--;
                }
                depth++;
            }
            return depth;
        }

        public static int Mod(int n, double d)
        {
            return (int)(n % d);
        }

        public static int Number(int n, double d)
        {
            return (int)(n / d);
        }
    }
}
