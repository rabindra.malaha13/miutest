﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S11N1SmallestContains2
{
    public class SmallestNumber
    {
        public static int Smallest(int n)
        {
            int number = 0;
            bool iterate = true;
            int smallestNumber = 123;
            while (iterate)
            {
                int[] array = { };
                for (int i = 1; i <= n; i++)
                {
                    if (Contains2(i * smallestNumber))
                    {
                        array = array.Append(i * smallestNumber).ToArray();
                        number = smallestNumber;
                        if (array.Length == n)
                        {
                            iterate = false;
                        }
                    }
                }

                smallestNumber++;
            }
            return number;
        }

        public static bool Contains2(int n)
        {
            while (n > 0)
            {
                int mode = n % 10;
                if (mode == 2)
                {
                    return true;
                }
                n = n / 10;
            }
            return false;
        }
    }
}
