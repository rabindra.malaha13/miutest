﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S11N3RailroadTie
{
    public class RailroadTie
    {
        public static int IsRailroaadTie(int[] array)
        {
            foreach (var item in array)
            {
                if (item > 0)
                {
                    break;
                }
            }
            for (int i = 0; i <= array.Length - 1; i++)
            {
                if (i == 0 || i == array.Length - 1)
                {
                    if (array[i] == 0) return 0;
                }
                else if (i <= array.Length - 2)
                {
                    if (array[i] == 0 && (array[i - 1] == 0 || array[i + 1] == 0))
                    {
                        return 0;
                    }
                    else if (array[i] != 0)
                    {
                        if (!(array[i - 1] != 0 ^ array[i + 1] != 0))
                            return 0;
                        if (i == array.Length - 3 || i == 2)
                            if ((array[i - 1] != 0 && array[i - 2] != 0) || (array[i + 1] != 0 && array[i + 2] != 0))
                                return 0;
                    }
                }
            }
            return 1;
        }
    }
}
