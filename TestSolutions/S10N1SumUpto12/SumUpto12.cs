﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S10N1SumUpto12
{
    public class SumUpto12
    {
        public static int CountRepresentations(int number)
        {
            int count = 0;
            for (int rupee20 = 0; rupee20 <= (number) / 20; rupee20++)
            {
                for (int rupee10 = 0; rupee10 <= (number - (rupee20 * 20)) / 10; rupee10++)
                {
                    for (int rupee5 = 0; rupee5 <= (number - (rupee10 * 10 + rupee20 * 20)) / 5; rupee5++)
                    {
                        for (int rupee2 = 0; rupee2 <= (number - (rupee5 * 5 + rupee10 * 10 + rupee20 * 20)) / 2; rupee2++)
                        {
                            for (int rupee1 = 0; rupee1 <= number - (rupee2 * 2 + rupee5 * 5 + rupee10 * 10 + rupee20 * 20); rupee1++)
                            {
                                if ((rupee1 + rupee2 * 2 + rupee5 * 5 + rupee10 * 10 + rupee20 * 20) == number)
                                {
                                    count++;
                                }
                            }
                        }
                    }
                }
            }
            return count;
        }

    }

}

