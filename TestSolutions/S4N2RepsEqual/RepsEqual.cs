﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S4N2RepsEqual
{
    public class RepsEqual
    {
        public static bool RepsEqualCheck(int[] array, int n)
        {
            double sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i] * Math.Pow(10, array.Length - 1 - i);
            }
            if (sum != n) return false;
            return true;
        }
    }
}
