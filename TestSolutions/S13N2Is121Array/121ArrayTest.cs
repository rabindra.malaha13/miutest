﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S13N2Is121Array
{
    public class ArrayTest
    {
        public static int Is121Array(int[] array)
        {
            if (array.Any(x => x > 2) || array.FirstOrDefault() != 1 || array.LastOrDefault() != 1) return 0;

            var twoIndex = Array.IndexOf(array, 2);
            var oneFirstArray = array.Take(twoIndex).ToArray();

            var nextArray = array.Skip(twoIndex).ToArray();
            var nextOneIndex = Array.IndexOf(nextArray, 1);

            var twoArray = nextArray.Take(nextOneIndex).ToArray();
            var nextOneArray = nextArray.Skip(nextOneIndex).ToArray();

            if (oneFirstArray.Length != nextOneArray.Length) return 0;
            if (nextOneArray.Contains(2)) return 0;
            return 1;
        }
    }
}
