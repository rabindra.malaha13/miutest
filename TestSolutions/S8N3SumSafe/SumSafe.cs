﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S8N3SumSafe
{
    public class SumSafe
    {
        public static int IsSumSafe(int[] A)
        {
            if (A.Contains(0)) return 0;
            int sum = A.Sum();
            if (A.Contains(sum)) return 0;
            return 1;
        }
    }
}
