﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.N2NUpcount
{
    public class NUpcount
    {
        public static int NUpCount(int[] array, int n)
        {
            int count = 0;
            int sum = 0;
            foreach (int i in array.ToList())
            {
                sum += i;
                if (sum > n) { count++; }
            }
            return count;
        }
    }
}
