﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S12N2Pack
{
    public class Pack
    {
        public static int IsPacked(int[] array)
        {
            int[] distinctArray = array.Distinct().ToArray();
            foreach (int i in distinctArray)
            {
                if (i != array.Count(x => x == i))
                {
                    return 0;
                }
                int tempIndex = Array.IndexOf(array, i);
                for (int j = 0; j < array.Length; j++)
                {
                    if (i == array[j])
                    {
                        if (j != tempIndex) return 0;
                        tempIndex++;
                    }
                }

            }

            return 1;
        }
    }
}
