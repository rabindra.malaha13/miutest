﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S3N2SumFactor
{
    public class SumFacrtor
    {
        public static int CountSumFact(int[] array)
        {
            int count = 0;
            int sum = array.Sum();
            for (int i = 0; i < array.Length; i++)
            {
                if (sum == array[i]) { count++; }
            }
            return count;
        }
    }
}
