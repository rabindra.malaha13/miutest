﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S16N1IntegerBasedRounding
{
    public class IntegerBasedRounding
    {
        public static int[] DoIntegerBasedRounding(int[] array, int n)
        {
            if (n <= 0) return array;
            int[] result = { };
            float subtractor = n / 2;
            if (n % 2 != 0)
                subtractor = subtractor + 1;
            foreach (int i in array)
            {
                if (i <= 0)
                {
                    result = result.Append(i).ToArray();
                }
                else
                {
                    if (i % n == 0)
                    {
                        result = result.Append(i).ToArray();
                    }
                    else
                    {
                        int rem = i % n;
                        int num = i / n;
                        if (rem < subtractor)
                        {
                            result = result.Append(num * n).ToArray();
                        }
                        else
                        {
                            result = result.Append((num + 1) * n).ToArray();
                        }
                    }
                }
            }
            return result;
        }
    }
}
