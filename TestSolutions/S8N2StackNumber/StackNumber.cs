﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S8N2StackNumber
{
    public class StackNumber
    {
        public static int IsStack(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int n = i + 1;
                int sum = n * (n + 1) / 2;
                if (array[i] != sum)
                {
                    return 0;
                }
            }
            return 1;
        }
    }
}
