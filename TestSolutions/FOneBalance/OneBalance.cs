﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.FOneBalance
{
    public class OneBalance
    {
        public static int IsOneBalance(int[] array)
        {
            if (array.Length == 0) return 1;
            int oneCount = array.Count(x => x == 1);
            int nonOneCount = array.Count(x => x != 1);
            if (oneCount != nonOneCount) return 0;
            int[] indexArray = { };
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 1) indexArray = indexArray.Append(i).ToArray();
            }
            int count = 0;
            for (int j = 1; j < indexArray.Length; j++)
            {
                if (indexArray[j] - indexArray[j - 1] > 1)
                {
                    count++;
                    for (int k = indexArray[j]; k < array.Length; k++)
                    {
                        if (array[k] != 1)
                        {
                            return 0;
                        }
                    }
                }
            }
            return 1;
        }
    }
}
