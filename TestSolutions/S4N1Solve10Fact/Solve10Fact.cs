﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S4N1Solve10Fact
{
    public class Solve10Fact
    {
        public static int[] Solve10()
        {
            int[] result = new int[2];
            int fact10 = 1;
            int factX = 1;
            int factY = 1;
            int x = 0;
            int y = 0;
            bool solved = false;
            for (int i = 1; i <= 10; i++)
            {
                fact10 *= i;
            }
            for (x = 1; x <= 10 && solved == false; x++)
            {
                factX *= x;
                for (y = 1; y <= 10 && solved == false; y++)
                {
                    factY *= y;
                    solved = (fact10 == factX + factY);
                }
            }
            if (!solved)
            {
                x = 0;
                y = 0;
            }
            result[0] = x;
            result[1] = y;
            return result;
        }
    }
}
