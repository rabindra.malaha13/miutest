﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S15N2EncodedNumber
{
    public class EncodedNumber
    {
        public static int[] EncodedNumbers(int n)
        {
            if (n <= 1) return null;
            int[] result = { };
            int divider = 2;
            while (n > 1)
            {
                if (n % divider == 0)
                {
                    result = result.Append(divider).ToArray();
                    n /= divider;
                }
                else
                {
                    divider++;
                }
            }
            return result;
        }
    }
}
