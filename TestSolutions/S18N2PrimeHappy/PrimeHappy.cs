﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S18N2PrimeHappy
{
    public class PrimeHappy
    {
        public static int IsPrimeHappy(int n)
        {
            if (n < 5) return 0;
            int[] array = { 2, 3 };
            for (int i = 5; i < n; i++)
            {
                if (CheckPrime(i))
                    array = array.Append(i).ToArray();
            }
            int sum = array.Sum();
            if (sum % n != 0) return 0;
            return 1;
        }

        private static bool CheckPrime(int n)
        {
            int i;
            for (i = 2; i < n; i++)
            {
                if (n % i == 0) return false;
            }
            if (i == n)
            {
                return true;
            }
            return true;
        }
    }
}
