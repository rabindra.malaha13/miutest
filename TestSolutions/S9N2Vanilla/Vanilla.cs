﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S9N2Vanilla
{
    public class Vanilla
    {
        public static int IsVanilla(int[] array)
        {
            int CHECK_NUMBER = 0;
            foreach (int i in array)
            {
                int n = Math.Abs(i);
                while (n > 0)
                {
                    var numbers = CheckNumber(n);
                    n = numbers.Item1;
                    if (CHECK_NUMBER == 0)
                    {
                        CHECK_NUMBER = numbers.Item2;
                    }
                    if (CHECK_NUMBER != numbers.Item2) return 0;

                }
            }
            return 1;
        }

        private static (int, int) CheckNumber(int n)
        {
            double mod = n % 10;
            double num = n / 10;
            return ((int)num, (int)mod);
        }
    }
}
