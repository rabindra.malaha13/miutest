﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S5N1PerfectPrime
{
    public class PerfectPrime
    {
        public static int Henry(int i, int j)
        {
            int fperfectPrime = 1;
            int pNum = 0;
            int sNum = 0;
            if (i < 0 && j < 0) return 0;
            int count = 1;
            while (count <= j)
            {
                int perfectPrime = PerfectPrimeNumber(fperfectPrime);
                if (count == i)
                {
                    pNum = perfectPrime;
                }
                fperfectPrime = perfectPrime + 1;
                if (count == j)
                    sNum = perfectPrime;
                count++;
            }
            return sNum + pNum;
        }

        //public static int Henry(int i, int j)
        //{
        //    double fNum = Math.Pow(2, i) * (Math.Pow(2, i + 1) - 1);
        //    double sNum = Math.Pow(2, j) * (Math.Pow(2, j + 1) - 1);

        //    return (int)(fNum + sNum);
        //}

        //perfect number = (2^p − 1) * 2^(p−1)
        public static int PerfectPrimeNumber(int n)
        {
            bool isPerfectPrime = true;
            while (isPerfectPrime)
            {
                int[] factor = { };
                for (int k = 1; k < n; k++)
                {
                    if (n % k == 0)
                    {
                        factor = factor.Append(k).ToArray();
                    }
                }
                if (factor.ToList().Sum() == n)
                {
                    break;
                }
                n++;
            }
            return n;
        }
    }
}
