﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S9N3Trivalent
{
    public class Trivalent
    {
        public static int IsTrivalent(int[] array)
        {
            int[] newArray = { };
            foreach (int i in array)
            {
                if (!newArray.Contains(i))
                    newArray = newArray.Append(i).ToArray();
                if (newArray.Length > 3)
                    return 0;
            }
            if (newArray.Length < 3)
                return 0;
            return 1;
        }
    }
}
