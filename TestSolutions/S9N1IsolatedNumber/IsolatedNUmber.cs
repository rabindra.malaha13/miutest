﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S9N1IsolatedNumber
{
    public class IsolatedNumber
    {
        public static int IsIsolatedNumber(int n)
        {
            double square = Math.Pow(n, 2);
            double cube = Math.Pow(n, 3);
            int cb = (int)cube;
            int sq = (int)square;
            int[] numberInSQ = { };
            int[] numberInCB = { };
            while (sq > 0)
            {
                int mod = sq % 10;
                sq /= 10;
                numberInSQ = numberInSQ.Append(mod).ToArray();
            }
            while (cb > 0)
            {
                int mod = cb % 10;
                cb /= 10;
                numberInCB = numberInCB.Append(mod).ToArray();
            }
            if (numberInCB.Intersect(numberInSQ).ToArray().Sum() > 0)
                return 0;
            return 1;
        }
    }
}
