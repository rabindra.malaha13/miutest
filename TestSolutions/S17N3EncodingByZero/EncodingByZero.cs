﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S17N3EncodingByZero
{
    public class EncodingByZero
    {
        public static int Encode(int[] array)
        {
            double number = 0;
            int mul = 0;
            int[] newArrayOfNumber = { };
            if (array[0] < 0)
            {
                mul = -1;
                array = array.Skip(1).ToArray();
            }
            int indexOf1 = Array.IndexOf(array, 1);
            while (array.Length > 0)
            {
                int[] newArray = array.Take(indexOf1 + 1).ToArray();
                array = array.Skip(indexOf1 + 1).ToArray();
                indexOf1 = Array.IndexOf(array, 1);
                int n = newArray.Count(x => x == 0);
                newArrayOfNumber = newArrayOfNumber.Append(n).ToArray();
            }


            for (int i = 0; i < newArrayOfNumber.Length; i++)
            {
                number += newArrayOfNumber[newArrayOfNumber.Length - 1 - i] * Math.Pow(10, i);
            }
            return (int)(mul * number);
        }
    }
}
