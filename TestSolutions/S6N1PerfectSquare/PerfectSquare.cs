﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S6N1PerfectSquare
{
    public class PerfectSquare
    {
        public static int IsPerfectSquare(int n)
        {
            if (n < 0) return 0;
            double sqrt = Math.Sqrt(n);
            if (sqrt % 1 != 0) return 0;
            return 1;
        }
    }
}
