﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S3N3GuthrieIndex
{
    public class GuthrieIndex
    {
        public static int FindGuthrieIndex(int n)
        {
            int[] newArray = { n };
            bool tr = newArray.Contains(1);
            while (!newArray.Contains(1))
            {
                if (n % 2 == 0)
                {
                    newArray = newArray.Append(n / 2).ToArray();
                    n /= 2;
                }
                else
                {
                    newArray = newArray.Append(3 * n + 1).ToArray();
                    n = 3 * n + 1;
                }
            }
            return newArray.Length - 1;
        }
    }
}
