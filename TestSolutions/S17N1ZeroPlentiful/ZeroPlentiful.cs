﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIUTestFrameApp.S17N1ZeroPlentiful
{
    public class ZeroPlentiful
    {
        public static int IsZeroPlentiful(int[] array)
        {
            int count = 0;
            int zeroCount = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 0)
                {
                    count++;
                    if (count == 4)
                    {
                        zeroCount++;
                        count = 0;
                    }
                }
                else
                {
                    count = 0;
                }
            }
            return zeroCount;
        }
    }
}
